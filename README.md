![Stitcher](/Stitcher.png)

# Stitcher #
Script for Adobe Photoshop to stitch images side by side.

### Installation ###
Clone or download this repository and place **Stitcher.jsx** script to Photoshop’s Scripts folder:

```Adobe Photoshop CC 20XX -> Presets -> Scripts -> Stitcher.jsx```

Restart Photoshop to access **Stitcher** script from File -> Scripts

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------