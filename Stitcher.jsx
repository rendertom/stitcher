/**********************************************************************************************
    Stitcher v0.2.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Select a folder containing PNG files.
		Script will stich them up.

	TODO:
		Add more save options,
		Control stitching direction
**********************************************************************************************/

(function () {
	buildUI();

	function buildUI() {
		var btnSize = 24,
			stSize = 65,
			etSize = 60;

		var win = new Window('dialog', "Stitcher");
		win.spacing = 5;
		win.alignChildren = ["fill", "fill"];

		win.group1 = win.add("group");
		win.stNumberOfFrames = win.group1.add('statictext', undefined, "Num of Frames");
		win.etNumberOfFrames = win.group1.add('edittext', undefined, "9");
		win.etNumberOfFrames.preferredSize.width = etSize;

		win.group2 = win.add("group");
		win.panLayout = win.group2.add("panel", undefined, "Layout");
		win.panLayout.alignChildren = ["fill", "fill"];
		win.panLayout.spacing = 5;
		win.group21 = win.panLayout.add("group");
		win.stRows = win.group21.add('statictext', undefined, "Rows");
		win.stRows.preferredSize.width = stSize;
		win.etRows = win.group21.add('edittext', undefined, "3");
		win.etRows.preferredSize.width = etSize;

		win.group21 = win.panLayout.add("group");
		win.stColumns = win.group21.add('statictext', undefined, "Columns");
		win.stColumns.preferredSize.width = stSize;
		win.etColumns = win.group21.add('edittext', undefined, "3");
		win.etColumns.preferredSize.width = etSize;

		win.panTileSize = win.group2.add("panel", undefined, "Tile Size");
		win.panTileSize.alignChildren = ["fill", "fill"];
		win.panTileSize.spacing = 5;
		win.group21 = win.panTileSize.add("group");
		win.stWidth = win.group21.add('statictext', undefined, "Width");
		win.stWidth.preferredSize.width = stSize;
		win.etWidth = win.group21.add('edittext', undefined, "4000");
		win.etWidth.preferredSize.width = etSize;

		win.group21 = win.panTileSize.add("group");
		win.stHeight = win.group21.add('statictext', undefined, "Height");
		win.stHeight.preferredSize.width = stSize;
		win.etHeight = win.group21.add('edittext', undefined, "4000");
		win.etHeight.preferredSize.width = etSize;

		win.group4 = win.add("group");
		win.stInputFolder = win.group4.add('statictext', undefined, "Input Folder");
		win.stInputFolder.preferredSize.width = 100;
		win.etInputFolder = win.group4.add('edittext', undefined, "");
		win.etInputFolder.preferredSize.width = 200;
		win.btnSelectInputFolder = win.group4.add('button', undefined, "...");
		win.btnSelectInputFolder.preferredSize.width = btnSize * 2;
		win.btnSelectInputFolder.onClick = function () {
			var inputFolder = Folder.selectDialog("Select PNG folder");
			if (inputFolder)
				win.etInputFolder.text = inputFolder.fsName;
		};

		win.group6 = win.add("group");
		win.group6.alignChildren = ["fill", "fill"];
		win.btnCloseWindow = win.group6.add('button', undefined, "Close");
		win.btnCloseWindow.onClick = function () {
			win.close();
		};
		win.btnRunScript = win.group6.add('button', undefined, "Run Script");
		win.btnRunScript.onClick = function () {
			if (win.etInputFolder.text !== "") {
				var inputFolder = Folder(win.etInputFolder.text);
				if (inputFolder.exists) {

					var filesArray = inputFolder.getFiles("*.png");
					if (filesArray.length === 0) {
						win.etInputFolder.text = "";
						return alert("Selected folder does not contain any PNG images. Please select another folder.");
					}
					win.close();
					var data = {
						numberOfFrames: parseInt(win.etNumberOfFrames.text),
						rows: parseInt(win.etRows.text),
						columns: parseInt(win.etColumns.text),
						tileSize: {
							width: parseInt(win.etWidth.text),
							height: parseInt(win.etHeight.text),
						},
					};
					main(data, filesArray.sort());
				} else {
					return alert("Folder does not exist:\n" + inputFolder);
				}
			}
		};

		win.show();
	}

	function main(data, filesArray) {

		try {

			var startRulerUnits, framesArray,
				mainDoc, tileDoc,
				tileLayer,
				posX, posY, rowIndex, frameNumber,
				psdFile;

			startRulerUnits = preferences.rulerUnits;
			preferences.rulerUnits = Units.PIXELS;

			if (filesArray.length > data.numberOfFrames * (data.rows * data.columns)) {
				filesArray.length = data.numberOfFrames * (data.rows * data.columns);
			}

			framesArray = [];
			while (filesArray.length > 0)
				framesArray.push(filesArray.splice(0, (data.rows * data.columns)));

			for (var f = 0, fl = framesArray.length; f < fl; f++) {
				frameNumber = f.toString();
				while (frameNumber.length < 3)
					frameNumber = "0" + frameNumber;

				mainDoc = app.documents.add(data.tileSize.width * data.columns, data.tileSize.height * data.rows, 72, "Frame " + frameNumber, NewDocumentMode.RGB);

				for (var i = 0, il = framesArray[f].length; i < il; i++) {
					tileDoc = open(framesArray[f][i]);
					tileLayer = tileDoc.backgroundLayer.duplicate(mainDoc, ElementPlacement.PLACEATEND);
					app.activeDocument = mainDoc;
					tileLayer.name = decodeURI(tileDoc.name.substring(0, tileDoc.name.lastIndexOf(".")));
					tileDoc.close(SaveOptions.DONOTSAVECHANGES);

					posX = data.tileSize.width * i;
					posY = rowIndex = 0;
					while (posX >= parseInt(mainDoc.width)) {
						rowIndex++;
						posX -= parseInt(mainDoc.width);
						posY = data.tileSize.height * rowIndex;
					}

					repositionLayer(tileLayer, posX, posY);
				}

				if (mainDoc.backgroundLayer)
					mainDoc.backgroundLayer.remove();

			}

			alert("Done");

		} catch (e) {
			alert(e.toString() + "\nLine: " + e.line.toString());
		}
	}

	function repositionLayer(layer, offsetX, offsetY) {
		var x = offsetX - layer.bounds[0].value;
		var y = offsetY - layer.bounds[1].value;

		layer.translate(x, y);
	}

	function getBaseName(document) {
		return decodeURI(document.name.substring(0, document.name.lastIndexOf(".")));
	}

})();